# Slugger 
A simple utility to turn any blog post title into url friendly slug.

## installation
```
npm i @roeesh/slugger
```
## Features
1. Turn any blog post title into url friendly slug.

## CommonJS
```javascript
const slugger = require('@roeesh/slugger'); 
```

## ES6 modules
```javascript
import slugger from '@roeesh/slugger'; 
```

## Usage
```javascript
slugger.slugger("I am","a","slugger test");
```