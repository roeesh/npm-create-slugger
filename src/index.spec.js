import * as s from "./index.js";

/**
 * @describe [optional] - group of tests with a header to describe them
 */
 describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(s.slugger("I am")).toEqual("I-am");
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        expect(s.slugger("I am","a","slugger test")).toEqual("I-am-a-slugger-test");
    })
})