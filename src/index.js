export function slugger(...args) {
    let resStr = "";
    args.forEach(str =>{
        str.split(" ").forEach(word=> resStr += (word +"-"));
    })
    return resStr.substring(0, resStr.length - 1);
}
